<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__047d631ff5f71b46c43687192c02ac62d21ff4b55b527d71c02ad017bdfdefd6 */
class __TwigTemplate_3d2a6df6d4c84825bb9f9de9a8fafeb2bf66545aa0933671efc9d5418506e6e6 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("set" => 7, "trans" => 9);
        $filters = array("escape" => 12);
        $functions = array("path" => 7);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'trans'],
                ['escape'],
                ['path']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
";
        // line 7
        $context["edit_content_types"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("entity.node_type.collection");
        // line 8
        echo "
";
        // line 9
        echo t("<p>The Field Example provides a field composed of an HTML RGB value, like
<code>#ff00ff</code>. To use it, <a href=@edit_content_types>add the
field to a content type</a>.</p>", array("@edit_content_types" =>         // line 12
($context["edit_content_types"] ?? null), ));
    }

    public function getTemplateName()
    {
        return "__string_template__047d631ff5f71b46c43687192c02ac62d21ff4b55b527d71c02ad017bdfdefd6";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 12,  70 => 9,  67 => 8,  65 => 7,  62 => 6,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__047d631ff5f71b46c43687192c02ac62d21ff4b55b527d71c02ad017bdfdefd6", "");
    }
}
