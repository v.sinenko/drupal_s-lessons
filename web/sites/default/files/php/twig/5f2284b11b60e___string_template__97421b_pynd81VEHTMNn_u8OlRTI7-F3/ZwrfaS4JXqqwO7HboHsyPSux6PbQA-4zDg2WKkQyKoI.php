<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__97421bf953a0f02c416bfa6964e0ed08585b6b77504d1dee6aa906cc10951d76 */
class __TwigTemplate_a9685d5388d9b975a3b1e51b0b155f506ff78229f1329480546341df9299826a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("trans" => 7);
        $filters = array();
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['trans'],
                [],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
";
        // line 7
        echo t("<h2>PHPUnit for Drupal: A very basic how-to.</h2>

<h3>How to use this example module</h3>
<p>You really should be reading the various docblocks in the test files.</p>

<h3>How To:</h3>
<ul>
<li><p>PHPUnit tests belong in their own directory, so they won&#39;t be loaded
  by the autoloader during normal bootstrap. This means you should have a
  <code>/tests/src</code> directory in the root of your module directory.</p>
</li>

<li><p>Your tests should be in the <code>Drupal\\Tests\\[your_module]\\Unit</code>
  namespace. Under Drupal&#39;s PSR-4 system, this means your PHPUnit-based
  tests should go in a <code>[your_module]/tests/src/Unit</code>
  directory.</p>
</li>
<li><p>Your test case should subclass <code>Drupal\\Tests\\UnitTestCase</code>.
   </p>
</li>
<li>
  <p>You can run PHPUnit-based tests from within Drupal 8 by enabling the
  Testing module and then selecting the PHPUnit group from the testing page.
  As of this writing, this method doesn&#39;t provide any useful output.
  </p>
</li>
</ul>

<h3>Standard PHPUnit Practices</h3>
<p>You can (and really, should) run PHPUnit from the command line.</p>
<p>On unix-based systems this means you need to <code>cd core</code> and then
  <pre><code>./vendor/bin/phpunit</code></pre>
</p>
<p>On Windows-based systems, assuming you have php in your path,
  <pre><code>php ./vendor/phpunit/phpunit/composer/bin/phpunit --group phpunit_example</code></pre>
</p>
<p>Also, you should mark your tests as belonging to a group, so they can be run
   independently. You do this by annotating your test classes with
   <code>@group group_name</code>. You should have a <code>@group</code> for
   your module name, and you should also have a <code>@group</code> for
   integrations, such as <code>views</code>.
</p>
<p>So, for instance, to run all of the PHPUnit example tests, you would type
  <pre><code>./vendor/bin/phpunit --group phpunit_example</code></pre>
</p>
<p>As you can see, including a <code>@group</code> annotation is a good idea.</p>", array());
    }

    public function getTemplateName()
    {
        return "__string_template__97421bf953a0f02c416bfa6964e0ed08585b6b77504d1dee6aa906cc10951d76";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 7,  62 => 6,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__97421bf953a0f02c416bfa6964e0ed08585b6b77504d1dee6aa906cc10951d76", "");
    }
}
