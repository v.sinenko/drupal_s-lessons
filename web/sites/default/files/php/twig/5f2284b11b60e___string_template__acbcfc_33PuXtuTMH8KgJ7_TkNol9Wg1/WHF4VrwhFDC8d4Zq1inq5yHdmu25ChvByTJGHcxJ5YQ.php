<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__acbcfc3e93947b7d8950e5b1091ceebccfa1a4fa85b5a90ae9bf580dc26bcaf5 */
class __TwigTemplate_925b5cc3ace39979062571cb3f7553c8f24abc0e10d13406c2b2afef4c7a12db extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("set" => 7, "trans" => 16);
        $filters = array("escape" => 24);
        $functions = array("path" => 7);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'trans'],
                ['escape'],
                ['path']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
";
        // line 7
        $context["custom_access"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("examples.menu_example.custom_access");
        // line 8
        $context["permissioned"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("examples.menu_example.permissioned");
        // line 9
        $context["route_only"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("examples.menu_example.route_only");
        // line 10
        $context["tabs"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("examples.menu_example.tabs");
        // line 11
        $context["use_url_arguments"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("examples.menu_example.use_url_arguments");
        // line 12
        $context["title_callbacks"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("examples.menu_example.title_callbacks");
        // line 13
        $context["placeholder_argument"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("examples.menu_example.placeholder_argument");
        // line 14
        $context["path_override"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("example.menu_example.path_override");
        // line 15
        echo "
";
        // line 16
        echo t("<p>This page is displayed by the simplest (and base) menu example. Note that
the title of the page is the same as the link title. There are a number of
examples here, from the most basic (like this one) to extravagant mappings of
loaded placeholder arguments. Enjoy!</p>

<ul>
    <li><a href=@custom_access>Custom Access Example</a></li>
    <li><a href=@permissioned>Permissioned Example</a></li>
    <li><a href=@route_only>Route only example</a></li>
    <li><a href=@tabs>Tabs</a></li>
    <li><a href=@use_url_arguments>URL Arguments</a></li>
    <li><a href=@title_callbacks>Dynamic title</a></li>
    <li><a href=@placeholder_argument>Placeholder Arguments</a></li>
    <li><a href=@path_override>Path Override</a></li>
</ul>", array("@custom_access" =>         // line 24
($context["custom_access"] ?? null), "@permissioned" =>         // line 25
($context["permissioned"] ?? null), "@route_only" =>         // line 26
($context["route_only"] ?? null), "@tabs" =>         // line 27
($context["tabs"] ?? null), "@use_url_arguments" =>         // line 28
($context["use_url_arguments"] ?? null), "@title_callbacks" =>         // line 29
($context["title_callbacks"] ?? null), "@placeholder_argument" =>         // line 30
($context["placeholder_argument"] ?? null), "@path_override" =>         // line 31
($context["path_override"] ?? null), ));
    }

    public function getTemplateName()
    {
        return "__string_template__acbcfc3e93947b7d8950e5b1091ceebccfa1a4fa85b5a90ae9bf580dc26bcaf5";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 31,  105 => 30,  104 => 29,  103 => 28,  102 => 27,  101 => 26,  100 => 25,  99 => 24,  84 => 16,  81 => 15,  79 => 14,  77 => 13,  75 => 12,  73 => 11,  71 => 10,  69 => 9,  67 => 8,  65 => 7,  62 => 6,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__acbcfc3e93947b7d8950e5b1091ceebccfa1a4fa85b5a90ae9bf580dc26bcaf5", "");
    }
}
