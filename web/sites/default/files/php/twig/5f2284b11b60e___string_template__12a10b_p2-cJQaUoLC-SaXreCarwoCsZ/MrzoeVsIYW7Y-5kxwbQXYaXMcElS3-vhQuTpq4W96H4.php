<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__12a10bb83209fae1f40ee2a329aca5e45be6db535043e3152e56a90540414900 */
class __TwigTemplate_4b5c7fd206b4f583d0a08ae69f9781ec6a87f496a14995b4044a3f79de914085 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("set" => 8, "trans" => 20);
        $filters = array("escape" => 7);
        $functions = array("attach_library" => 7, "path" => 8);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'trans'],
                ['escape'],
                ['attach_library', 'path']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
";
        // line 7
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\Core\Template\TwigExtension']->attachLibrary("core/drupal.dialog.ajax"), "html", null, true);
        echo "
";
        // line 8
        $context["simple_form"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("form_api_example.simple_form");
        // line 9
        $context["multistep_form"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("form_api_example.multistep_form");
        // line 10
        $context["input_demo"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("form_api_example.input_demo");
        // line 11
        $context["build_demo"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("form_api_example.build_demo");
        // line 12
        $context["container_demo"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("form_api_example.container_demo");
        // line 13
        $context["state_demo"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("form_api_example.state_demo");
        // line 14
        $context["vertical_tabs_demo"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("form_api_example.vertical_tabs_demo");
        // line 15
        $context["ajax_demo"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("form_api_example.ajax_color_demo");
        // line 16
        $context["ajax_addmore"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("form_api_example.ajax_addmore");
        // line 17
        $context["modal_form"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("form_api_example.modal_form", ["nojs" => "nojs"]);
        // line 18
        $context["block_admin"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("block.admin_display");
        // line 19
        echo "
";
        // line 20
        echo t("<p>Form examples to demonstrate common UI solutions using the Drupal Form API</p>
<p><a href=@simple_form>Simple form</a></p>
<p><a href=@multistep_form>Multistep form</a></p>
<p><a href=@input_demo>Common input elements</a></p>
<p><a href=@build_demo>Build form demo</a></p>
<p><a href=@container_demo>Container elements</a></p>
<p><a href=@state_demo>Form state binding</a></p>
<p><a href=@vertical_tabs_demo>Vertical tab elements</a></p>
<p><a href=@ajax_demo>Ajax form</a></p>
<p><a href=@ajax_addmore>Add-more button</a></p>
<p><a href=@modal_form>Modal form</a></p>

<p>This module also provides a block, \"Example: Display a form\" that
    demonstrates how to display a form in a block. This same technique can be
    used whenever you need to display a form that is not the primary content of
    a page. You can enable it on your site <a href=@block_admin>using the
    block admin page</a>.</p>", array("@simple_form" =>         // line 22
($context["simple_form"] ?? null), "@multistep_form" =>         // line 23
($context["multistep_form"] ?? null), "@input_demo" =>         // line 24
($context["input_demo"] ?? null), "@build_demo" =>         // line 25
($context["build_demo"] ?? null), "@container_demo" =>         // line 26
($context["container_demo"] ?? null), "@state_demo" =>         // line 27
($context["state_demo"] ?? null), "@vertical_tabs_demo" =>         // line 28
($context["vertical_tabs_demo"] ?? null), "@ajax_demo" =>         // line 29
($context["ajax_demo"] ?? null), "@ajax_addmore" =>         // line 30
($context["ajax_addmore"] ?? null), "@modal_form" =>         // line 31
($context["modal_form"] ?? null), "@block_admin" =>         // line 36
($context["block_admin"] ?? null), ));
    }

    public function getTemplateName()
    {
        return "__string_template__12a10bb83209fae1f40ee2a329aca5e45be6db535043e3152e56a90540414900";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  121 => 36,  120 => 31,  119 => 30,  118 => 29,  117 => 28,  116 => 27,  115 => 26,  114 => 25,  113 => 24,  112 => 23,  111 => 22,  94 => 20,  91 => 19,  89 => 18,  87 => 17,  85 => 16,  83 => 15,  81 => 14,  79 => 13,  77 => 12,  75 => 11,  73 => 10,  71 => 9,  69 => 8,  65 => 7,  62 => 6,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__12a10bb83209fae1f40ee2a329aca5e45be6db535043e3152e56a90540414900", "");
    }
}
