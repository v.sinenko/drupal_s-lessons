<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__e3cece410d5ca69ae41c7091a916870a8197159feff49fe77054038dc243ef70 */
class __TwigTemplate_ca62190c170694bf134fa090ad6d5fc7a43a30846fc4f53e3ea02db6d4857339 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("set" => 8, "trans" => 13);
        $filters = array("escape" => 18);
        $functions = array("path" => 8);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'trans'],
                ['escape'],
                ['path']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "
";
        // line 8
        $context["simple_rows"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("tabledrag_example.simple_form");
        // line 9
        $context["nested"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("tabledrag_example.parent_form");
        // line 10
        $context["roots_and_leaves"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("tabledrag_example.rootleaf_form");
        // line 11
        $context["reset_form"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("tabledrag_example.reset_form");
        // line 12
        echo "
";
        // line 13
        echo t("<p>Below are examples of Drupal 8's table element with 'tabledrag' functionality.</p>

<ol>
    <li><a href=@simple_rows>Simple Rows</a></li>
    <li><a href=@nested>Nested</a></li>
    <li><a href=@roots_and_leaves>Roots and Leaves</a></li>
</ol>

<p><a href=@reset_form>Reset Tablesort Sample Data</a></p>", array("@simple_rows" =>         // line 18
($context["simple_rows"] ?? null), "@nested" =>         // line 19
($context["nested"] ?? null), "@roots_and_leaves" =>         // line 20
($context["roots_and_leaves"] ?? null), "@reset_form" =>         // line 23
($context["reset_form"] ?? null), ));
    }

    public function getTemplateName()
    {
        return "__string_template__e3cece410d5ca69ae41c7091a916870a8197159feff49fe77054038dc243ef70";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 23,  87 => 20,  86 => 19,  85 => 18,  76 => 13,  73 => 12,  71 => 11,  69 => 10,  67 => 9,  65 => 8,  62 => 7,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__e3cece410d5ca69ae41c7091a916870a8197159feff49fe77054038dc243ef70", "");
    }
}
