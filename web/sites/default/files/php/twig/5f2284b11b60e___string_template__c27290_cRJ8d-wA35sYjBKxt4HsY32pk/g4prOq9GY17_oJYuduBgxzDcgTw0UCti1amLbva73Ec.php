<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__c272906764e3ccf0a5037100447bc105427475b68c3653526cc4ad55fc9f6e84 */
class __TwigTemplate_91368ebdb9ec5a74e199a989bf26d2acde071378a254dda12a21f3315b8fbd0d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("trans" => 7);
        $filters = array();
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['trans'],
                [],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
";
        // line 7
        echo t("<p>The Config Entity Example module defines a Robot entity type. This is a list
  of the Robot entities currently in your Drupal site.</p><p>By default, when
  you enable this module, one entity is created from configuration. This is
  why we call them Config Entities. Marvin, the paranoid android, is created
  in the database when the module is enabled.</p><p>You can view a list of
  Robots here. You can also use the 'Operations' column to edit and delete
  Robots.</p>", array());
    }

    public function getTemplateName()
    {
        return "__string_template__c272906764e3ccf0a5037100447bc105427475b68c3653526cc4ad55fc9f6e84";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 7,  62 => 6,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__c272906764e3ccf0a5037100447bc105427475b68c3653526cc4ad55fc9f6e84", "");
    }
}
