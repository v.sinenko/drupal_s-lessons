<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/examples/field_permission_example/templates/description.html.twig */
class __TwigTemplate_3b3210f6725a3098e5262fc3e6e69277e2ad2c3a4100db230c669442fb28c45b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("set" => 11, "trans" => 13);
        $filters = array("escape" => 21);
        $functions = array("path" => 11);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'trans'],
                ['escape'],
                ['path']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "
";
        // line 11
        $context["edit_content_types"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("entity.node_type.collection");
        // line 12
        echo "
";
        // line 13
        echo t("<p>The Field Permission Example module shows how you can restrict view and edit
permissions within your field implementation. It adds a new field type called
Fieldnote. Fieldnotes appear as simple text boxes on the create/edit form, and
as sticky notes when viewed. By 'sticky note' we mean 'Post-It Note' but that's
a trademarked term.</p>

<p>To see this field in action, <a href=@edit_content_types>add it to a
content type</a> or user profile. Go to the permissions page (@admin_link)
and look at the 'Field Permission Example' section. This allows you to change
which roles can see and edit Fieldnote fields.</p>

<p>Creating different users with different capabilities will let you see these
behaviors in action. Fieldnote helpfully displays a message telling you which
permissions it is trying to resolve for the current field/user combination.</p>

<p>Definitely look through the code to see various implementation details.</p>", array("@edit_content_types" =>         // line 21
($context["edit_content_types"] ?? null), "@admin_link" =>         // line 22
($context["admin_link"] ?? null), ));
    }

    public function getTemplateName()
    {
        return "modules/examples/field_permission_example/templates/description.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 22,  86 => 21,  70 => 13,  67 => 12,  65 => 11,  62 => 10,);
    }

    public function getSourceContext()
    {
        return new Source("", "modules/examples/field_permission_example/templates/description.html.twig", "/var/www/2.student.drupal-coder.ru/data/www/2.student.drupal-coder.ru/web/modules/examples/field_permission_example/templates/description.html.twig");
    }
}
