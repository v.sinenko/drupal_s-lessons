<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__e59e34285ee2b7cd24cad69b3a06e7088b2af945362f8950b81cc785a13db581 */
class __TwigTemplate_974cb0b7e3cf6a8c21531b03c8080160d4c0c233baa41dcaaf451227cbc3d53f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("set" => 7, "trans" => 10);
        $filters = array("escape" => 20);
        $functions = array("path" => 7);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'trans'],
                ['escape'],
                ['path']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
";
        // line 7
        $context["arrays_url"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("render_example.arrays");
        // line 8
        $context["alter_url"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("render_example.altering");
        // line 9
        echo "
";
        // line 10
        echo t("<h2>What are render arrays?</h2>

<p>
  These are examples of how to construct render arrays.</p>

<p>
  You use render arrays to specify to Drupal how to construct HTML content.</p>

<ul>
  <li><a href=\"@arrays_url\">Demonstration of render array usage.</a></li>
  <li><a href=\"@alter_url\">Using alter hooks to modify existing render arrays.</a></li>
</ul>", array("@arrays_url" =>         // line 20
($context["arrays_url"] ?? null), "@alter_url" =>         // line 21
($context["alter_url"] ?? null), ));
    }

    public function getTemplateName()
    {
        return "__string_template__e59e34285ee2b7cd24cad69b3a06e7088b2af945362f8950b81cc785a13db581";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 21,  84 => 20,  72 => 10,  69 => 9,  67 => 8,  65 => 7,  62 => 6,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__e59e34285ee2b7cd24cad69b3a06e7088b2af945362f8950b81cc785a13db581", "");
    }
}
