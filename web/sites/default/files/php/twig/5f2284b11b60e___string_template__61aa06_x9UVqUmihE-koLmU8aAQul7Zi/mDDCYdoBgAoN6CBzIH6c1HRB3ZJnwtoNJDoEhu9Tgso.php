<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__61aa064c63b6e3f740db9002c82e4c813b00815d7fc932edf08a60c9f30e31e4 */
class __TwigTemplate_a2944894c8fda889f6e53fd504725dffca69d6c41ad365e98f33ce5b92165d90 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("set" => 8, "trans" => 11);
        $filters = array("escape" => 13);
        $functions = array("path" => 8);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'trans'],
                ['escape'],
                ['path']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "
";
        // line 8
        $context["page_example_simple"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("page_example.simple");
        // line 9
        $context["page_example_arguments"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("page_example.arguments", ["first" => 23, "second" => 56]);
        // line 10
        echo "
";
        // line 11
        echo t("<p>The Page example module provides two pages, \"simple\" and \"arguments\".</p>
<p>The <a href=@page_example_simple>simple page</a> just returns a renderable array for display.</p>
<p>The <a href=@page_example_arguments>arguments page</a> takes two arguments and displays them, as in @page_example_arguments</p>", array("@page_example_simple" =>         // line 13
($context["page_example_simple"] ?? null), "@page_example_arguments" =>         // line 14
($context["page_example_arguments"] ?? null), "@page_example_arguments" => ($context["page_example_arguments"] ?? null), ));
    }

    public function getTemplateName()
    {
        return "__string_template__61aa064c63b6e3f740db9002c82e4c813b00815d7fc932edf08a60c9f30e31e4";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 14,  75 => 13,  72 => 11,  69 => 10,  67 => 9,  65 => 8,  62 => 7,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__61aa064c63b6e3f740db9002c82e4c813b00815d7fc932edf08a60c9f30e31e4", "");
    }
}
