<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__9a603b90578e3446dfb61129696d4e68aaffe0e487cc1a2baf96732ae8fe74f8 */
class __TwigTemplate_81b6dc422edb9e5dc4ec6c43d727fd9d9491f7a8c72c9ca12cd13d42ff2e351a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("set" => 7, "trans" => 9);
        $filters = array("escape" => 13);
        $functions = array("url" => 7);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'trans'],
                ['escape'],
                ['url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
";
        // line 7
        $context["block_admin_page"] = (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $this->extensions['Drupal\Core\Template\TwigExtension']->getUrl("block.admin_display")) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["#markup"] ?? null) : null);
        // line 8
        echo "
";
        // line 9
        echo t("<p>The Block Example provides three sample blocks which demonstrate the various
  block APIs. To experiment with the blocks, enable and configure them on
  <a href=\"@block_admin_page\">the block admin page</a>.</p>", array("@block_admin_page" =>         // line 13
($context["block_admin_page"] ?? null), ));
    }

    public function getTemplateName()
    {
        return "__string_template__9a603b90578e3446dfb61129696d4e68aaffe0e487cc1a2baf96732ae8fe74f8";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  70 => 9,  67 => 8,  65 => 7,  62 => 6,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__9a603b90578e3446dfb61129696d4e68aaffe0e487cc1a2baf96732ae8fe74f8", "");
    }
}
