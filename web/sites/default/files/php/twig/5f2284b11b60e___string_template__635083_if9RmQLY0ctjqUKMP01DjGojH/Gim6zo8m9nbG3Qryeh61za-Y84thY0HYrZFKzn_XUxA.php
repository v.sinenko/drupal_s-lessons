<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__635083f2e74dfb6f5313826a59b9b3625918e164882c2222079167372ffc92fb */
class __TwigTemplate_c6b99614da68b21c5a4e1dc11fa15b95eb8b14aa5b48b0f54f293dbfd2c440c6 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("set" => 7, "trans" => 10);
        $filters = array("escape" => 18);
        $functions = array("path" => 7);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'trans'],
                ['escape'],
                ['path']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
";
        // line 7
        $context["js_weights"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("js_example.weights");
        // line 8
        $context["js_accordion"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("js_example.accordion");
        // line 9
        echo "
";
        // line 10
        echo t("<p>Drupal includes jQuery and jQuery UI.</p>

<p>We have two examples of using these:</p>

<ol>
  <li>
    <p><a href=@js_accordion>An accordion-style section reveal effect</a>:
        This demonstrates calling a JavaScript function using Drupal rendering
        system.
  </li>
  <li>
    <p><a href=@js_weights>Sorting according to numeric weight</a>: This
        demonstrates attaching your own JavaScript code to individual page
        elements using Drupal rendering system.</p>
  </li>
</ol>", array("@js_accordion" =>         // line 18
($context["js_accordion"] ?? null), "@js_weights" =>         // line 23
($context["js_weights"] ?? null), ));
    }

    public function getTemplateName()
    {
        return "__string_template__635083f2e74dfb6f5313826a59b9b3625918e164882c2222079167372ffc92fb";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 23,  88 => 18,  72 => 10,  69 => 9,  67 => 8,  65 => 7,  62 => 6,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__635083f2e74dfb6f5313826a59b9b3625918e164882c2222079167372ffc92fb", "");
    }
}
