<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__d97337381866145c77db1ac80c4d1fcd6a8bc26d839bf1647c342457cd36827b */
class __TwigTemplate_50b0bc2ab60022d9affdb18a5832ed78d9dfb61d09adfde808c277f982240a2b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("trans" => 7);
        $filters = array();
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['trans'],
                [],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
";
        // line 7
        echo t("<p>Please note that the use of SimpleTest is deprecated. This example module will
be removed in Drupal 9, and new tests should not be written using SimpleTest. In
addition, all existing SimpleTest tests should be converted to PHPUnit
functional tests.</p>

<p>
There are some instructions for how to convert Simpletest-based tests to the new
BrowserTestBase in this change notice:
<a href=\"https://www.drupal.org/node/2469723\">https://www.drupal.org/node/2469723</a>.
</p>", array());
    }

    public function getTemplateName()
    {
        return "__string_template__d97337381866145c77db1ac80c4d1fcd6a8bc26d839bf1647c342457cd36827b";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 7,  62 => 6,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__d97337381866145c77db1ac80c4d1fcd6a8bc26d839bf1647c342457cd36827b", "");
    }
}
